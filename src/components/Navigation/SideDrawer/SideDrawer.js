import React, { useState } from 'react'
import styled from 'styled-components'

import Logo from '../../Logo/Logo';
import Hamburger from './Hamburger/Hamburger'
import NavItems from '../NavItems/NavItems'

const FixedWrapper = styled.div `
    position: fixed;
    padding: 0 2rem;
    background-color: var(--color-main);
    z-index: 10;
    top: 0;
    left: 0;
    width: 100%;
    height: 6rem;
    display: none;
    @media ${props => props.theme.mediaQueries.smallest} {
        display: flex;
    }
`

const Wrapper = styled.div `
    display: flex;
    justify-content: space-between;
    height: 100%;
    align-items: center;
    width: 100%;
`

const Menu = styled.div `
    width: 100%;
    transform: translateY(${props => (props.opened ? '100%' : '0%')});
    height: 100vh;
    position: fixed;
    top: 0;
    left: 0;
    display: flex;
    align-content: center;
    justify-content: center;
    margin-top: 6rem;
    background-color: var(--color-mainDark);
    visibility: ${props => (props.opened ? 'visibile' : 'hidden')};
    transform: translateY(${props => (props.opened ? '0%' : '-100%')});
    transition: all 0.1s cubic-bezier(0.445, 0.05, 0.55, 0.95);
    display: none;
    @media ${props => props.theme.mediaQueries.smallest} {
        display: flex;
        z-index: 1;

    }
`

const SideDrawer = () => {
    const [ isOpened, setIsOpened ] = useState(false);
    return (
        <>
            <FixedWrapper>
                <Wrapper>
                    <Logo />
                    <Hamburger opened={isOpened} clicked={() => setIsOpened(!isOpened)}/>
                </Wrapper>
            </FixedWrapper>
            <Menu opened={isOpened}>
                <NavItems mobile clicked={() => setIsOpened(false)}/>
            </Menu>
        </>
    )
}

export default SideDrawer
