import React from 'react'
import { Formik, Field } from 'formik'
import * as Yup from 'yup'

import { FormWrapper, StyledForm } from '../../../hoc/layout/elements'
import Input from '../../../components/UI/Forms/Input/Input'
import Button from '../../../components/UI/Forms/Button/Button'
import Heading from '../../../components/UI/Headings/Heading'

const LoginSchema = Yup.object().shape({
    email: Yup.string().email('invalid email.').required('The email is required.'),
    password: Yup.string().required('The password is required.')
})

const Login = () => {
    return (
        <Formik 
            initialValues={{
                email: '',
                password: ''
            }}
            validationSchema={LoginSchema}
            onSubmit={(values, {setSubmitting}) => console.log(values)}
        >
            {({isSubmitting, isValid}) => (
                <FormWrapper>
                    <Heading 
                        size="h1"
                        bold
                        color="white"
                        noMargin
                    >Login into your account</Heading>
                    <Heading
                        size="h4"
                        bold
                        color="white"
                    >
                        Fill in your details to login into your account
                    </Heading>
                    <StyledForm>
                        <Field 
                            name="email"
                            type="email"
                            placeholder="Your email..."
                            component={Input}
                        />

                        <Field 
                            name="password"
                            type="password"
                            placeholder="Your password..."
                            component={Input}
                        />
                        <Button disabled={!isValid} type="submit" >Submit</Button>
                    </StyledForm>
                </FormWrapper>
            )}
        </Formik>
    )
}

export default Login
