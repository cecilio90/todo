import React from 'react'
import { Formik, Field } from 'formik'
import * as Yup from 'yup'

import { FormWrapper, StyledForm } from '../../../hoc/layout/elements'
import Input from '../../../components/UI/Forms/Input/Input'
import Button from '../../../components/UI/Forms/Button/Button'
import Heading from '../../../components/UI/Headings/Heading'

const SignUpSchema = Yup.object().shape({
    firstName: Yup.string().required('The first name is required').min(3, 'Too short').max(25, 'Too long'),
    lastName: Yup.string().required('The last name is required').min(3, 'Too short').max(25, 'Too long'),
    email: Yup.string().email('invalid email.').required('The email is required.'),
    password: Yup.string().required('The password is required.'),
    confirmPassword: Yup.string().oneOf([Yup.ref('password'), null], `Password doesn't match`).required('You must confirn your password') 
})

const SignUp = () => {
    return (
        <Formik 
            initialValues={{
                firstName: '',
                lastName: '',
                email: '',
                password: '',
                confirmPassword: ''
            }}
            validationSchema={SignUpSchema}
            onSubmit={(values, {setSubmitting}) => console.log(values)}
        >
            {({isSubmitting, isValid}) => (
                <FormWrapper>
                    <Heading 
                        size="h1"
                        bold
                        color="white"
                        noMargin
                    >SignUp for an account</Heading>
                    <Heading
                        size="h4"
                        bold
                        color="white"
                    >
                        Fill in your details to register your new account
                    </Heading>
                    <StyledForm>
                        <Field 
                            name="firstName"
                            type="text"
                            placeholder="Your first name..."
                            component={Input}
                        />
                        <Field 
                            name="lastName"
                            type="text"
                            placeholder="Your last name..."
                            component={Input}
                        />
                        <Field 
                            name="email"
                            type="email"
                            placeholder="Your email..."
                            component={Input}
                        />
                        <Field 
                            name="password"
                            type="password"
                            placeholder="Your password..."
                            component={Input}
                        />
                        <Field 
                            name="confirmPassword"
                            type="password"
                            placeholder="confirm your password"
                            component={Input}
                        />
                        <Button disabled={!isValid} type="submit" >SignUp</Button>
                    </StyledForm>
                </FormWrapper>
            )}
        </Formik>
    )
}

export default SignUp
