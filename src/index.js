import React from 'react'
import ReactDOM from 'react-dom'

import { ThemeProvider } from 'styled-components'
import { BrowserRouter } from 'react-router-dom';

import theme from './utils/theme';
import GlobalStyles from './utils/global'

import { Provider } from 'react-redux'

import store from './store'
import App from './App'

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <>
          <App />
          <GlobalStyles />
        </>
      </ThemeProvider>
    </BrowserRouter>
  </Provider>
    
, document.querySelector('#root'));