import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const config = {
  apiKey: process.env.REACT_APP_APIKEY,
  authDomain: "todo-auth-ff394.firebaseapp.com",
  databaseURL: "https://todo-auth-ff394.firebaseio.com",
  projectId: "todo-auth-ff394",
  storageBucket: "todo-auth-ff394.appspot.com",
  messagingSenderId: "1021406790180",
  appId: "1:1021406790180:web:8816d19bf7108672"
}

firebase.initializeApp(config);
firebase.firestore();

export default firebase;